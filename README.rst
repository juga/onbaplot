OnBaPlot
============

Create graphs from Tor bandwidth list files v1.0.0 and v1.1.0
or raw measurements files.

Download
--------

Clone the project with Git by running::

    git clone https://gitlab.torproject.org/juga/onbaplot

Installation
------------

    python setup.py install

Bugs and features
-----------------

To report a bug or a feature request, please fill in an issue on the
`onbaplot issue tracker <https://gitlab.torproject.org/juga/onbaplot/issues>`__.

License
-------

``onbaplot`` is Copyright 2018 by juga (juga at riseup dot net)
under the terms of the
`CC0 1.0 <https://creativecommons.org/publicdomain/zero/1.0/>`__ license.
